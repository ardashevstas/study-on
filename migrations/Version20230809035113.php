<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230809035113 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE course ADD type SMALLINT NULL');
        $this->addSql('ALTER TABLE course ADD cost DOUBLE PRECISION NULL');
        $this->addSql('ALTER TABLE course ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE lesson ALTER id DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE lesson_id_seq');
        $this->addSql('SELECT setval(\'lesson_id_seq\', (SELECT MAX(id) FROM lesson))');
        $this->addSql('ALTER TABLE lesson ALTER id SET DEFAULT nextval(\'lesson_id_seq\')');
        $this->addSql('ALTER TABLE course DROP type');
        $this->addSql('ALTER TABLE course DROP cost');
        $this->addSql('CREATE SEQUENCE course_id_seq');
        $this->addSql('SELECT setval(\'course_id_seq\', (SELECT MAX(id) FROM course))');
        $this->addSql('ALTER TABLE course ALTER id SET DEFAULT nextval(\'course_id_seq\')');
    }
}
