<?php

namespace App\Security;

use App\Exception\BillingUnavailableException;
use App\Service\BillingClient;
use App\Service\DecodeTokenService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class BillingAuthenticator extends AbstractLoginFormAuthenticator
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'app_login';

    private UrlGeneratorInterface $urlGenerator;

    private BillingClient $billingClient;

    private DecodeTokenService $decodeTokenService;

    public function __construct(UrlGeneratorInterface $urlGenerator, BillingClient $billingClient, DecodeTokenService $decodeTokenService)
    {
        $this->urlGenerator  = $urlGenerator;
        $this->billingClient = $billingClient;
        $this->decodeTokenService = $decodeTokenService;
    }

    public function authenticate(Request $request): Passport
    {
        $email = $request->request->get('email', '');
        $password = $request->request->get('password', '');

        $request->getSession()->set(Security::LAST_USERNAME, $email);

        try {
            $billingResponse = $this->billingClient->auth($email, $password);

            $token = $billingResponse['token'];
            $decodedPayload = $this->decodeTokenService->decodeToken($token);

            $roles = $decodedPayload['roles'];

            // Создаем пользователя на основе ответа
            $user = new User();
            $user->setEmail($email);
            $user->setToken($token);
            $user->setRoles($roles);
            $user->setRefreshToken($billingResponse['refresh_token']);
        } catch (BillingUnavailableException $exception) {
            throw new CustomUserMessageAuthenticationException($exception->getMessage());
        }

        return new Passport(
            new UserBadge($email, function () use ($user) {
                return $user;
            }),
            new CustomCredentials(
                function ($credentials, UserInterface $user) {
                    return !empty($user->getToken());
                },
                [
                    'email'    => $email,
                    'password' => $password,
                ]
            ),
            [
                new CsrfTokenBadge('authenticate', $request->request->get('_csrf_token')),
                new RememberMeBadge()
            ]
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $firewallName)) {
            return new RedirectResponse($targetPath);
        }

        return new RedirectResponse($this->urlGenerator->generate('app_course_index'));

    }

    protected function getLoginUrl(Request $request): string
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }


}
