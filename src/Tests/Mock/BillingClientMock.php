<?php

namespace App\Tests\Mock;

use App\Security\User;
use App\Service\BillingClient;

class BillingClientMock extends BillingClient
{
    public function __construct()
    {
        // Ничего не делаем в конструкторе, так как мы не хотим вызывать конструктор родительского класса
    }

    private $mockToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2ODc3MDA4MzksImV4cCI6MTY4NzcwNDQzOSwicm9sZXMiOlsiUk9MRV9TVVBFUl9BRE1JTiIsIlJPTEVfVVNFUiJdLCJ1c2VybmFtZSI6ImFyZGFzaGV2c3Rhc0BnbWFpbC5jb20ifQ.XVmvFkH6NcEbZRDoghMpfCk3_VTrWTEuy4M-1QqQnPAtu_pmYKkHiltFzueCnPjY9t2D6krJgjvkK_UAjQ-h_RI5a4Y2ifAjv-nXG-A9Y5Oktesv7fG44R5GEobf83gVpK0BnHa_K3eueuAmyJA3_qjQzVNVViVfjVjonlEqlzg07FoZqrvlXxn-qiIs19QUh2n0HDDO-tPpfp_BKiLdM7PrmA0buss_WUH07xZFePuAo4Ypnjx8a0OfrnO3GQAQi5lPyFG9JzOlrQjxtcStqWX3-RvboCG3vIMVn3yOAsFzUdymfPuvXsHe5VJQPjA8sTm3EXZ0GydS13pRij_uFj8eUN68xvI9n4ua8iHD9-yJrQNafz11MJb7tDWbUaeHbjuoZqvpP7aUvany_mUQiKo37Ltc15vIQOwHVlcKH_tMtcW8BWfMALC9DfXS7c7uhtTLXF9u3cXCuxONAuSNhoeA3MqZLyr0LM1Li3bB0Iicff4-nXb1PfiBtU22JT14HQdW-UeWkZy9EWmLEoQoscY1kZd4MjkMS0BgG-OXlon96HdkgOoA0lCOspQITXFkUmjcztgSjnugICq5Gt-yPv5EMss80fIm54gcWFxwPhOJOor9U9l5mUCqUJH0jq2rydwZ0QKNj6JRrf0S4BoAKEFgW9_Y4W5m84u94WQBjBk';

    public function register(string $email, string $password): User
    {
        // Здесь вы можете имитировать регистрацию, вернуть фиктивный пользовательский объект или что-то еще
        $user = new User();
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setToken($this->mockToken);
        return $user;
    }

    public function auth(string $email, string $password): array
    {

        return [
            'token' => $this->mockToken
        ];
    }
}
