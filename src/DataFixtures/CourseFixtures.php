<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Course;
use App\Entity\Lesson;

class CourseFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $courseData = [
            [
                'code' => 'python',
                'title' => 'Python',
                'description' => 'Изучите основы программирования на Python, универсальном языке, используемом в data science и AI.',
                'lessons' => [
                    ['Переменные и типы данных', 'Изучите переменные и типы данных в Python.'],
                    ['Управление потоком', 'Познакомьтесь с управлением потоком в Python.'],
                    ['Функции', 'Научитесь создавать функции в Python.'],
                    ['Классы и объекты', 'Изучите основы ООП в Python.'],
                    ['Модули и пакеты', 'Познакомьтесь с модулями и пакетами в Python.'],
                ],
                'type' => 3,
                'cost' => 25000
            ],
            [
                'code' => 'vuejs',
                'title' => 'Vue.js',
                'description' => 'Освойте искусство front-end разработки с Vue.js, популярным JavaScript-фреймворком.',
                'lessons' => [
                    ['Введение в Vue.js', 'Изучите основы работы с Vue.js.'],
                    ['Компоненты', 'Научитесь создавать компоненты в Vue.js.'],
                    ['Роутинг', 'Познакомьтесь с системой роутинга в Vue.js.'],
                    ['Состояние приложения', 'Изучите управление состоянием в Vue.js.'],
                    ['HTTP-запросы', 'Познакомьтесь с основами работы с HTTP-запросами в Vue.js.'],
                ],
                'type' => 1,
                'cost' => 6000
            ],
            [
                'code' => 'php',
                'title' => 'PHP',
                'description' => 'Изучите основы серверного языка PHP, используемого для создания веб-приложений.',
                'lessons' => [
                    ['Синтаксис PHP', 'Изучите базовый синтаксис PHP.'],
                    ['Управление данными', 'Научитесь работать с переменными и массивами в PHP.'],
                    ['Формы и PHP', 'Изучите обработку форм с использованием PHP.'],
                    ['Сессии и куки', 'Познакомьтесь с сессиями и куками в PHP.'],
                    ['Работа с базами данных', 'Изучите основы работы с базами данных в PHP.'],
                ],
                'type' => 1,
                'cost' => 7000
            ],
            [
                'code' => 'java',
                'title' => 'Java',
                'description' => 'Изучите основы программирования на Java, универсальном языке, используемом в различных областях.',
                'lessons' => [
                    ['Синтаксис Java', 'Изучите базовый синтаксис Java.'],
                    ['ООП в Java', 'Научитесь основам объектно-ориентированного программирования на Java.'],
                    ['Исключения', 'Познакомьтесь с системой исключений в Java.'],
                    ['Работа с файлами', 'Изучите основы работы с файлами в Java.'],
                    ['Java и базы данных', 'Познакомьтесь с основами работы с базами данных в Java.'],
                ],
                'type' => 1,
                'cost' => 8000
            ],
            [
                'code' => 'go',
                'title' => 'Go',
                'description' => 'Освойте Go, мощный язык, разработанный в Google, для создания масштабируемых и эффективных приложений.',
                'lessons' => [
                    ['Синтаксис Go', 'Изучите базовый синтаксис Go.'],
                    ['Структуры данных', 'Научитесь работать со структурами данных в Go.'],
                    ['Функции', 'Познакомьтесь с функциями в Go.'],
                    ['Конкурентность', 'Изучите модель конкурентности в Go.'],
                    ['Работа с сетью', 'Познакомьтесь с основами работы с сетью в Go.'],
                ],
                'type' => 3,
                'cost' => 70000
            ],
            [
                'code' => 'timemanagment',
                'title' => 'Тайм-менеджмент',
                'description' => 'Бесплатный курс по управлению временем',
                'lessons' => [
                    ['Урок 1', 'Повысите свою эффективность в личной жизни и работе.'],
                    ['Урок 2', 'Сосредоточитесь на самом важном'],
                    ['Урок 3', 'Найдёте время на себя и близких, отдых и хобби.']
                ],
                'type' => 2,
                'cost' => 0
            ],
            [
                'code' => 'reactjs',
                'title' => 'React.js',
                'description' => 'Освойте основы создания интерфейсов с помощью React.js.',
                'lessons' => [
                    ['Введение в React.js', 'Изучите основы React.js.'],
                    ['Компоненты и пропсы', 'Работа с компонентами и пропсами в React.js.'],
                    ['Состояние и жизненный цикл', 'Управление состоянием компонентов.'],
                    ['Хуки', 'Использование хуков для написания более чистого кода.'],
                    ['React Router', 'Настройка маршрутизации в вашем приложении.'],
                ],
                'type' => 1,
                'cost' => 7500
            ],
            [
                'code' => 'nodejs',
                'title' => 'Node.js',
                'description' => 'Освойте серверный JavaScript с помощью Node.js.',
                'lessons' => [
                    ['Введение в Node.js', 'Что такое Node.js и зачем он нужен.'],
                    ['Обработка асинхронных операций', 'Работа с промисами и async/await.'],
                    ['Работа с файловой системой', 'Чтение и запись файлов.'],
                    ['Express.js', 'Создание веб-сервера с помощью Express.js.'],
                    ['Базы данных', 'Работа с базами данных в Node.js.'],
                ],
                'type' => 3,
                'cost' => 30000
            ],
            [
                'code' => 'typescript',
                'title' => 'TypeScript',
                'description' => 'Строго типизированный JavaScript.',
                'lessons' => [
                    ['Введение в TypeScript', 'Зачем нужен TypeScript.'],
                    ['Базовые типы', 'Работа с базовыми типами в TypeScript.'],
                    ['Интерфейсы', 'Создание и использование интерфейсов.'],
                    ['Классы', 'ООП в TypeScript.'],
                    ['Декораторы', 'Расширение функциональности с помощью декораторов.'],
                ],
                'type' => 1,
                'cost' => 6500
            ],
            [
                'code' => 'sql',
                'title' => 'SQL',
                'description' => 'Освойте язык запросов SQL для работы с базами данных.',
                'lessons' => [
                    ['Введение в SQL', 'Базовые команды и синтаксис.'],
                    ['Запросы данных', 'Изучите SELECT, WHERE, ORDER BY и другие.'],
                    ['Модификация данных', 'INSERT, UPDATE, DELETE и т.д.'],
                    ['Соединения', 'INNER JOIN, OUTER JOIN и их разновидности.'],
                    ['Агрегация данных', 'GROUP BY, HAVING и агрегатные функции.'],
                ],
                'type' => 1,
                'cost' => 5500
            ],
            [
                'code' => 'css3',
                'title' => 'CSS3',
                'description' => 'Продвинутые возможности стилизации веб-страниц.',
                'lessons' => [
                    ['Введение в CSS3', 'Новые возможности и особенности.'],
                    ['Анимации и переходы', 'Создание плавных анимаций и переходов.'],
                    ['Flexbox', 'Современный способ верстки с помощью flexbox.'],
                    ['Grid', 'Работа с сеткой и макетами.'],
                    ['Отзывчивый дизайн', 'Создание адаптивных веб-страниц.'],
                ],
                'type' => 2,
                'cost' => 0
            ]
        ];

        foreach ($courseData as $index => $data) {
            $course = new Course();
            $course->setCode($data['code']);
            $course->setTitle($data['title']);
            $course->setDescription($data['description']);
            $course->setType($data['type']);
            $course->setCost($data['cost']);
            $manager->persist($course);

            foreach ($data['lessons'] as $key => $lessonData) {
                $lesson = new Lesson();
                $lesson->setCourse($course);
                $lesson->setTitle($lessonData[0]);
                $lesson->setContent($lessonData[1]);
                $lesson->setNumber($key);
                $manager->persist($lesson);
            }
        }

        $manager->flush();
    }
}
