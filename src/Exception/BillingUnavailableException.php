<?php

namespace App\Exception;

use Throwable;

class BillingUnavailableException extends \Exception
{
    const SERVICE_UNAVAILABLE = 503;
    const OTHER_ERROR = 500;

    private $errorCode;

    public function __construct(
        string $message = "",
        int $errorCode = self::OTHER_ERROR,
        int $code = 0,
        Throwable $previous = null
    ) {
        $this->errorCode = $errorCode;
        parent::__construct($message, $code, $previous);
    }

    public function getErrorCode(): int
    {
        return $this->errorCode;
    }
}
