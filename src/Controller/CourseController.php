<?php

namespace App\Controller;

use App\Entity\Course;
use App\Exception\BillingUnavailableException;
use App\Form\CourseType;
use App\Repository\CourseRepository;
use App\Repository\LessonRepository;
use App\Service\BillingClient;
use App\Service\CoursesService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/courses")
 */
class CourseController extends AbstractController
{
    private CourseRepository $courseRepository;
    private LessonRepository $lessonRepository;
    private CoursesService $coursesService;
    private BillingClient $billingClient;

    public function __construct(CourseRepository $courseRepository, LessonRepository $lessonRepository, CoursesService $coursesService, BillingClient $billingClient)
    {
        $this->courseRepository = $courseRepository;
        $this->lessonRepository = $lessonRepository;
        $this->coursesService = $coursesService;
        $this->billingClient = $billingClient;
    }

    /**
     * @Route("/", name="app_course_index", methods={"GET"})
     */
    public function index(): Response
    {
        $courses = $this->coursesService->getCoursesData();

        return $this->render('course/index.html.twig', [
            'courses' => $courses
        ]);
    }

    /**
     * @Route("/new", name="app_course_new", methods={"GET", "POST"})
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function new(Request $request): Response
    {
        $course = new Course();
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);
        $errorMessage = null;


        if ($form->isSubmitted() && $form->isValid()) {

            try {
                // генерация символьного кода из названия
                $course->setCode($this->coursesService->generateCourseCode($course->getTitle()));

                // надо бы проверить нет ли курса с таким кодом
                $existingCourse = $this->courseRepository->findByCode($course->getCode());

                if ($existingCourse) {
                    $form->addError(new FormError('Курс с таким символьным кодом уже существует.'));
                    return $this->renderForm('course/new.html.twig', [
                        'course' => $course,
                        'form' => $form,
                        'error' => $errorMessage,
                    ]);
                }

                $this->coursesService->addCourse($course);

                $this->courseRepository->add($course, true);
                return $this->redirectToRoute('app_course_index', [], Response::HTTP_SEE_OTHER);
            } catch (BillingUnavailableException $e) {
                // Здесь вы можете обработать исключение и передать сообщение об ошибке
                $errorMessage = $e->getMessage();
            } catch (\InvalidArgumentException $e) {
                $errorMessage = $e->getMessage();
            }
        }

        return $this->renderForm('course/new.html.twig', [
            'course' => $course,
            'form' => $form,
            'error' => $errorMessage, // передайте сообщение об ошибке в шаблон
        ]);
    }


    /**
     * @Route("/{id}", name="app_course_show", methods={"GET"})
     */
    public function show(Course $course): Response
    {
        $lessons = $this->lessonRepository->findBy(
            ['course' => $course],
            ['number' => 'ASC']
        );

        $courseData = $this->coursesService->getCourseData($course);

        return $this->render('course/show.html.twig', [
            'course' => $courseData['course'],
            'lessons' => $lessons,
            'balance' => $courseData['balance']
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_course_edit", methods={"GET", "POST"})
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function edit(Request $request, Course $course): Response
    {
        $form = $this->createForm(CourseType::class, $course);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {

                $oldCode = $course->getCode();
                // генерация символьного кода из названия
                $course->setCode($this->coursesService->generateCourseCode($course->getTitle()));

                $this->coursesService->editCourse($course, $oldCode);

                $this->courseRepository->add($course, true);

                return $this->redirectToRoute('app_course_show', ['id' => $course->getId()], Response::HTTP_SEE_OTHER);

            } catch (BillingUnavailableException $e) {
                $errorMessage = $e->getMessage();
            } catch (\InvalidArgumentException $e) {
                $errorMessage = $e->getMessage();
            }
        }

        return $this->renderForm('course/edit.html.twig', [
            'course' => $course,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_course_delete", methods={"POST"})
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function delete(Request $request, Course $course): Response
    {
        if ($this->isCsrfTokenValid('delete'.$course->getId(), $request->request->get('_token'))) {
            $this->courseRepository->remove($course, true);
        }

        return $this->redirectToRoute('app_course_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/pay/{code}", name="app_course_pay")
     */
    public function pay(string $code): Response
    {
        $res = $this->billingClient->payCourse($code);

        if (array_key_exists('error' , $res)) {
            $this->addFlash(
                'error',
                $res['error']['message']
            );
        }

        $this->addFlash(
            'success', // Используйте тип 'success' для позитивных сообщений
            'Курс успешно оплачен'
        );

        $course = $this->courseRepository->findByCode($code);

        if (!$course) {
            throw $this->createNotFoundException('No course found for code ' . $code);
        }

        return $this->redirectToRoute('app_course_show', ['id' => $course->getId()]);
    }
}
