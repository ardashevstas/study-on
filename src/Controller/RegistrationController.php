<?php

namespace App\Controller;

use App\Exception\BillingUnavailableException;
use App\Form\RegistrationFormType;
use App\Security\BillingAuthenticator;
use App\Security\User;
use App\Service\BillingClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

class RegistrationController extends AbstractController
{
    private UserAuthenticatorInterface $authenticator;
    private BillingAuthenticator $formAuthenticator;
    private BillingClient $billingClient;

    public function __construct(
        UserAuthenticatorInterface $authenticator,
        BillingAuthenticator $formAuthenticator,
        BillingClient $billingClient
    )
    {
        $this->authenticator = $authenticator;
        $this->formAuthenticator = $formAuthenticator;
        $this->billingClient = $billingClient;
    }

    /**
     * @Route("/registration", name="app_registration")
     */
    public function register(Request $request): Response {

        // Проверяем, если пользователь уже авторизован, перенаправляем на страницу профиля
        if ($this->getUser()) {
            return $this->redirectToRoute('profile');
        }

        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                $user = $this->billingClient->register($user->getEmail(), $user->getPassword());
            } catch (BillingUnavailableException $exception) {
                $error = $exception->getMessage();

                // Отрисовка формы с ошибкой
                return $this->render('registration/register.html.twig', [
                    'registrationForm' => $form->createView(),
                    'error' => $error,
                ]);
            }

            // Аутентификация пользователя
            return $this->authenticator->authenticateUser(
                $user,
                $this->formAuthenticator,
                $request
            );
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
            'error' => $error ?? null,
        ]);
    }
}
