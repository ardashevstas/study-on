<?php

namespace App\Controller;

use App\Entity\Lesson;
use App\Form\LessonType;
use App\Repository\CourseRepository;
use App\Repository\LessonRepository;
use App\Service\CoursesService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use function PHPUnit\Framework\isNull;

/**
 * @Route("/lessons")
 */
class LessonController extends AbstractController
{
    private CourseRepository $courseRepository;
    private LessonRepository $lessonRepository;
    private CoursesService $coursesService;

    public function __construct(
        CourseRepository $courseRepository,
        LessonRepository $lessonRepository,
        CoursesService $coursesService
    ) {
        $this->courseRepository = $courseRepository;
        $this->lessonRepository = $lessonRepository;
        $this->coursesService = $coursesService;
    }

    /**
     * @Route("/", name="app_lesson_index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->redirectToRoute('app_course_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/new/{course_id?}", name="app_lesson_new", methods={"GET", "POST"})
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function new(Request $request, int $course_id): Response
    {
        $lesson = new Lesson();

        $form = $this->createForm(LessonType::class, $lesson);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $course = $this->courseRepository->find($course_id);
            $lesson->setCourse($course);
            $this->lessonRepository->add($lesson, true);

            return $this->redirectToRoute('app_course_show', ['id' => $course_id], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('lesson/new.html.twig', [
            'lesson' => $lesson,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_lesson_show", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function show(Lesson $lesson): Response
    {
        $course = $lesson->getCourse();
        $courseData = $this->coursesService->getCourseData($course);

        if (!$courseData['course']['status'] && $courseData['course']['type'] !== 'free') {
            throw new AccessDeniedException('У вас нет доступа к этому уроку.');
        }

        return $this->render('lesson/show.html.twig', [
            'lesson' => $lesson,
            'course' => $course
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_lesson_edit", methods={"GET", "POST"})
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function edit(Request $request, Lesson $lesson): Response
    {
        $course = $lesson->getCourse();

        $form = $this->createForm(LessonType::class, $lesson);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $lesson->setCourse($course);

            $this->lessonRepository->add($lesson, true);

            return $this->redirectToRoute('app_lesson_show', ['id' => $lesson->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('lesson/edit.html.twig', [
            'lesson' => $lesson,
            'form' => $form,
            'course' => $course
        ]);
    }

    /**
     * @Route("/{id}", name="app_lesson_delete", methods={"POST"})
     * @IsGranted("ROLE_SUPER_ADMIN")
     */
    public function delete(Request $request, Lesson $lesson): Response
    {
        $course = $lesson->getCourse();

        if ($this->isCsrfTokenValid('delete'.$lesson->getId(), $request->request->get('_token'))) {
            $this->lessonRepository->remove($lesson, true);
        }

        if (is_null($course)) {
            return $this->redirectToRoute('app_course_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->redirectToRoute('app_course_show', ['id' => $course->getId()], Response::HTTP_SEE_OTHER);
    }
}
