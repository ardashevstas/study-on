<?php

namespace App\Controller;

use App\Exception\BillingUnavailableException;
use App\Service\BillingClient;
use App\Service\TransactionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    private BillingClient $billingClient;
    private TransactionService $transactionService;

    public function __construct(BillingClient $billingClient, TransactionService $transactionService)
    {
        $this->billingClient = $billingClient;
        $this->transactionService = $transactionService;
    }

    /**
     * @Route("/profile", name="profile")
     */
    public function index(): Response
    {
        $currentUser = $this->billingClient->getCurrentUser();

        return $this->render('profile/index.html.twig', [
            'user' => $currentUser,
        ]);
    }

    /**
     * @Route("/profile/transactions", name="transactions")
     */
    public function transactions(): Response
    {
        $user = $this->getUser();

        if ($user) {
            $transactions = $this->transactionService->prepareTransactionsHistory($user->getToken());
        } else {
            throw new \RuntimeException('User is not authenticated.');
        }

        return $this->render('profile/transactions.html.twig', [
            'transactions' => $transactions,
        ]);
    }
}
