<?php

namespace App\Service;

use App\Exception\BillingUnavailableException;

class HttpClient
{
    private $billingServerUrl;
    private $headers = ['Content-Type: application/json'];
    public function __construct(string $billingServerUrl)
    {
        $this->billingServerUrl = $billingServerUrl;
    }

    public function sendRequest(string $method, string $url, array $postFields, string $token)
    {
        $headers = ['Content-Type: application/json'];

        if ($token) {
            $headers[] = 'Authorization: Bearer ' . $token;
        }

        $cUrlParams = [
            CURLOPT_URL            => $this->billingServerUrl . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => '',
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => $method,
            CURLOPT_HTTPHEADER     => $headers
        ];

        if (!empty($postFields)) {
            $cUrlParams[CURLOPT_POSTFIELDS] = json_encode($postFields);
        }

        $curl = curl_init();

        curl_setopt_array($curl, $cUrlParams);

        $response = json_decode(curl_exec($curl), true);

        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        $result = [
            'success' => in_array($httpCode, [200, 201]),
            'data' => $response
        ];

        if(!$result['success']) {
            $result['error'] = [
                'code' => $httpCode,
                'message' => $response['error']['message']
            ];
        }

        return $result;
    }
}