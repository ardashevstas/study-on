<?php

namespace App\Service;

use App\Entity\Course;
use App\Repository\CourseRepository;
use DateTime;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\AsciiSlugger;

class CoursesService
{
    private CourseRepository $courseRepository;
    private BillingClient $billingClient;
    private Security $security;
    private array $groupedTransactions = [];

    public function __construct(CourseRepository $courseRepository, BillingClient $billingClient, Security $security)
    {
        $this->courseRepository = $courseRepository;
        $this->billingClient = $billingClient;
        $this->security = $security;
    }

    public function getCourseData(Course $course)
    {
        $code = $course->getCode();
        $courseApiData = $this->billingClient->getCourseByCode($code);
        $courseDb = $this->courseRepository->findByCode($code);
        if (empty($courseDb)) {
            throw new \RuntimeException("Курс с кодом $code не найден.");
        }

        $user = $this->security->getUser();
        $this->getTransactions($user);

        $balance = 0;
        if ($user) {
            $balance = $this->billingClient->getCurrentUser()['balance'];
        }

        $result = [
            'course' => $this->prepareCourseData($courseApiData, $courseDb),
            'balance' => $balance
        ];

        return $result;
    }

    public function getCoursesData()
    {
        $coursesApiData = $this->billingClient->getCourses();
        $coursesDb = $this->courseRepository->findAll();
        if (empty($coursesDb)) {
            throw new \RuntimeException("Курсы не найдены.");
        }

        $groupCourses = [];
        foreach ($coursesApiData as $courseApi) {
            $groupCourses[$courseApi['code']] = $courseApi;
        }

        $user = $this->security->getUser();

        $this->getTransactions($user);

        $result = [];
        foreach ($coursesDb as $course) {
            $code = $course->getCode();
            $apiCourse = isset($groupCourses[$code]) ? $groupCourses[$code] : [];
            $result[] = $this->prepareCourseData($apiCourse, $course);
        }

        return $result;
    }

    private function prepareCourseData(array $courseApiData, Course $courseDb)
    {
        $id = $courseDb->getId();
        $code = $courseDb->getCode();
        $title = $courseDb->getTitle();
        $description = $courseDb->getDescription();
        $type = 'free';
        $price = 0;
        $status = null;

        if (!empty($courseApiData) && $code === $courseApiData['code']) {
            $type = $courseApiData['type'];
            $price = $courseApiData['price'];

            if (array_key_exists($code, $this->groupedTransactions)) {
                $status = $this->enrichCourseDataWithTransaction($courseApiData, $this->groupedTransactions[$code]);
            }
        }

        return [
            'id' => $id,
            'code' => $code,
            'title' => $title,
            'description' => $description,
            'type' => $type,
            'price' => $price,
            'status' => $status
        ];
    }

    private function getTransactions($user)
    {
        if ($user) {
            $this->groupedTransactions = $this->getGroupedTransactionsByCourseCode($user);
        }
    }

    private function getGroupedTransactionsByCourseCode($user): array
    {
        $userToken = $user->getToken();
        $transactions = $this->billingClient->getTransactions($userToken, '?filter[type]=payment');

        if (empty($transactions['data'])) {
            return [];
        }

        $groupedTransactions = [];
        foreach ($transactions['data'] as $transaction) {
            $courseCode = $transaction['course_code'];
            if (!isset($groupedTransactions[$courseCode])) {
                $groupedTransactions[$courseCode] = $transaction;
            } else {
                // Арендовать курс можно на 7 дней, и соотвественно один курс можно арендовать несколько раз, поэтому берем самую последнуюю аренду
                if (isset($transaction['expiration']) && isset($groupedTransactions[$courseCode]['expiration'])) {
                    $currentExpiry = new DateTime($groupedTransactions[$courseCode]['expiration']['date']);
                    $newExpiry = new DateTime($transaction['expiration']['date']);

                    if ($newExpiry > $currentExpiry) {
                        $groupedTransactions[$courseCode] = $transaction;
                    }
                }
            }
        }

        return $groupedTransactions;
    }

    private function enrichCourseDataWithTransaction(array $courseData, array $transaction): ?string
    {
        $until = null;
        if ($courseData['type'] === 'rent') {
            if (isset($transaction['expiration']) && !empty($transaction['expiration'])) {
                $expirationDate = new DateTime($transaction['expiration']['date']);
                $today = new DateTime();

                if ($expirationDate >= $today) {
                    $formattedDate = $expirationDate->format('Y-m-d');
                    $until = $formattedDate;
                }
            }
        } else {
            $until = 'bought';
        }

        $courseData['until'] = $until;
        return $courseData['until'];
    }

    public function addCourse(Course $course)
    {
        $courseArray = $this->courseToArray($course);

        return $this->billingClient->addCourse($courseArray);
    }

    private function courseToArray(Course $course): array {
        return [
            'type' => $course->getType(),
            'title' => $course->getTitle(),
            'code' => $course->getCode(),
            'price' => $course->getCost()
        ];
    }

    public function generateCourseCode(string $title): string
    {
        $slugger = new AsciiSlugger();
        return $slugger->slug($title)->lower();
    }

    public function editCourse(Course $course, string $code)
    {
        $courseArray = $this->courseToArray($course);

        return $this->billingClient->editCourse($courseArray, $code);
    }
}