<?php
namespace App\Service;

use App\Exception\BillingUnavailableException;
use App\Security\User;
use DateTime;
use Symfony\Component\Security\Core\Security;

class BillingClient
{
    private Security $security;
    private HttpClient $httpClient;

    public function __construct(Security $security, HttpClient $httpClient)
    {
        $this->security = $security;
        $this->httpClient = $httpClient;
    }

    public function register(string $email, string $password): User
    {
        $params = [
            'email'    => $email,
            'password' => $password
        ];

        $response = $this->httpClient->sendRequest('POST', '/api/v1/register', $params, '');

        if (!$response['success']) {
            throw new BillingUnavailableException('Error: ' . $response['error']['message']);
        }

        $user = new User();
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setToken($response['data']['token']);
        $user->setRefreshToken($response['data']['refresh_token']);

        return $user;
    }

    public function auth(string $email, string $password): array
    {
        $params = [
            'email'    => $email,
            'password' => $password
        ];

        $response = $this->httpClient->sendRequest('POST', '/api/v1/auth', $params, '');

        if (!$response['success']) {
            throw new BillingUnavailableException('Error: ' . $response['error']['message']);
        }

        return $response['data'];
    }

    public function getCurrentUser(): array
    {
        $user = $this->security->getUser();

        if ($user) {
            $userToken = $user->getToken();

            $response = $this->httpClient->sendRequest('GET', '/api/v1/users/current', [], $userToken);

            if (!$response['success']) {
                throw new BillingUnavailableException('Error: ' . $response['error']['message']);
            }
        } else {
            throw new \RuntimeException('User is not authenticated.');
        }

        return $response['data'];
    }

    public function refreshToken(string $refreshToken): string
    {
        if (!empty($refreshToken)) {

            $params = [
                'refresh_token' => $refreshToken
            ];

            $response = $this->httpClient->sendRequest('POST', '/api/v1/token/refresh', $params, '');

            if (!$response['success']) {
                throw new BillingUnavailableException('Error: ' . $response['error']['message']);
            }
        } else {
            throw new \InvalidArgumentException('Invalid refresh token.');
        }

        return $response['data']['token'];
    }

    public function getCourses()
    {
        $response = $this->httpClient->sendRequest('GET', '/api/v1/courses', [], '');

        if (!$response['success']) {
            if ($response['error']['code'] == 404) {
                return [];
            } else {
                throw new BillingUnavailableException('Error: ' . $response['error']['message']);
            }
        }

        return $response['data']['courses'];
    }

    public function getCourseByCode(?string $code)
    {
        $response = $this->httpClient->sendRequest('GET', '/api/v1/courses/' . $code, [], '');

        if (!$response['success']) {
            if ($response['error']['code'] == 404) {
                return [];
            } else {
                throw new BillingUnavailableException('Error: ' . $response['error']['message']);
            }
        }

        return $response['data']['course'];
    }

    public function getTransactions($userToken, $filter = ''): array
    {
        $response = $this->httpClient->sendRequest('GET', '/api/v1/transactions' . $filter, [], $userToken);

        if (!$response['success']) {
            throw new BillingUnavailableException('Error: ' . $response['error']['message']);
        }

        return $response['data'];
    }

    public function payCourse(string $code)
    {
        $token = $this->security->getUser()->getToken();

        $response = $this->httpClient->sendRequest('POST', '/api/v1/courses/' . $code . '/pay', [], $token);

        if (!$response['success']) {
            return $response;
        }

        return $response['data'];
    }

    public function addCourse(array $params): string
    {
        $token = $this->security->getUser()->getToken();

        if (!empty($params)) {

            $response = $this->httpClient->sendRequest('POST', '/api/v1/courses', $params, $token);

            if (!$response['success']) {
                throw new BillingUnavailableException('Error: ' . $response['error']['message']);
            }
        } else {
            throw new \InvalidArgumentException('Parameters are empty.');
        }

        return $response['data']['success'];
    }

    public function editCourse(array $params, string $code): string
    {
        $token = $this->security->getUser()->getToken();

        if (!empty($params)) {

            $response = $this->httpClient->sendRequest('POST', '/api/v1/courses/'. $code, $params, $token);

            if (!$response['success']) {
                throw new BillingUnavailableException('Error: ' . $response['error']['message']);
            }
        } else {
            throw new \InvalidArgumentException('Parameters are empty.');
        }

        return $response['data']['success'];
    }
}
