<?php

namespace App\Service;

class DecodeTokenService
{
    public function decodeToken(string $token): array
    {
        list($header, $payload, $signature) = explode('.', $token);

        return json_decode(base64_decode(strtr($payload, '-_', '+/')), true);
    }
}