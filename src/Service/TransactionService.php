<?php

namespace App\Service;

use App\Repository\CourseRepository;

class TransactionService
{
    private $courseRepository;
    private BillingClient $billingClient;

    public function __construct(CourseRepository $courseRepository, BillingClient $billingClient)
    {
        $this->courseRepository = $courseRepository;
        $this->billingClient = $billingClient;
    }

    public function prepareTransactionsHistory(string $token): array
    {
        $transactions = $this->billingClient->getTransactions($token);

        $courseCodes = array_column($transactions['data'], 'course_code');
        $courses = $this->courseRepository->findBy(['code' => array_unique($courseCodes)]);

        $coursesByCode = [];
        foreach ($courses as $course) {
            $coursesByCode[$course->getCode()] = $course;
        }

        foreach ($transactions['data'] as $index => $transaction) {
            if (isset($transaction['course_code'], $coursesByCode[$transaction['course_code']])) {
                $transactions['data'][$index]['course_id'] = $coursesByCode[$transaction['course_code']]->getId();
                $transactions['data'][$index]['title'] = $coursesByCode[$transaction['course_code']]->getTitle();
            }
        }

        return $transactions;
    }
}