<?php

namespace App\Tests;

use App\Controller\RegistrationController;
use App\Form\RegistrationFormType;
use App\Security\BillingAuthenticator;
use App\Security\User;
use App\Service\BillingClient;
use App\Tests\Mock\BillingClientMock;
use App\Tests\Util\TestUser;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Component\Form\Form;


class AuthRegControllerTest extends AbstractTest
{
    /*
     * Тест проверки авторизации с заглушкой:
     * 1 - проверяется что прошла аутентификация и создан пасспорт
     * 2 - происходит редирект на страницу курсов
     */
    public function testSuccessfulAuthenticationAndRedirect(): void
    {
        $client = self::getClient();

        // запрещаем перезагрузку ядра, чтобы не сбросилась подмена сервиса при запросе
        $client->disableReboot();

        // Подменяем сервис на заглушку 'App\Service\BillingClient'
        $client->getContainer()->set(
            'test.billingClient',
            new BillingClientMock()
        );

        // Создадаем поддельный запрос
        $request = new Request([], ['email' => 'ardashevstas@gmail.com', 'password' => 'ardashev']);
        $session = $this->createMock(Session::class);
        $request->setSession($session);

        // Создаем подделку для зависимости UrlGeneratorInterface
        $urlGenerator = $this->createMock(UrlGeneratorInterface::class);

        // Установливаем ожидаемое поведение для поддельного объекта
        // Здесь мы ожидаем, что метод generate будет вызван с аргументом 'app_course_index'
        $urlGenerator->expects($this->once())
            ->method('generate')
            ->with('app_course_index')
            ->willReturn('/courses');

        // Создаем экземпляр BillingAuthenticator с подмененным BillingClient
        $authenticator = new BillingAuthenticator($urlGenerator, $client->getContainer()->get('test.billingClient'));

        // Вызываем метод authenticate и получаем Passport
        $passport = $authenticator->authenticate($request);

        // Проверяем, что объект Passport был создан
        $this->assertNotNull($passport);

        // Вызываем метод и проверяем результат
        $response = $authenticator->onAuthenticationSuccess($request, $this->createMock(TokenInterface::class), 'main');

        // Проверить, что ответ является редиректом на ожидаемый URL
        $this->assertEquals('/courses', $response->getTargetUrl());
    }

    // TODO
//    public function testRegister()
//    {
//
//    }

}