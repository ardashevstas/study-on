<?php

namespace App\Tests;

use App\DataFixtures\CourseFixtures;
use App\Entity\Course;
use App\Repository\CourseRepository;
use App\Repository\LessonRepository;
use App\Security\BillingAuthenticator;
use App\Service\BillingClient;
use App\Tests\Mock\BillingClientMock;
use Symfony\Bundle\FrameworkBundle\Test\TestContainer;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CourseControllerTest extends AbstractTest
{
//    protected function tearDown(): void
//    {
//        $entityManager = static::$container->get('doctrine.orm.default_entity_manager');
//        $connection = $entityManager->getConnection();
//
//        // Сброс идентификаторов и удаление записей из таблицы lesson
//        $connection->executeStatement('TRUNCATE TABLE lesson RESTART IDENTITY CASCADE');
//
//        // Сброс идентификаторов и удаление записей из таблицы course
//        $connection->executeStatement('TRUNCATE TABLE course RESTART IDENTITY CASCADE');
//
//        parent::tearDown();
//    }
//
    /*
     * Проверяем, что на странице списка курсов именно столько курсов, сколько и в тестовой БД
     */
    public function testCoursesWithoutAuthentication()
    {
        // Получаем EntityManager
        $entityManager = $this->getEntityManager();

        // Вычисляем количество курсов в базе данных
        $courseCount = $entityManager->getRepository(Course::class)->count([]);

        $crawler = $this->getClient()->request('GET', '/courses/');

        $this->assertResponseOk();

        // Каждый курс представлен элементом <div> с классами "col-md-6" и "card mb-4"
        $courses = $crawler->filter('.col-md-6 .card.mb-4');

        // Проверяем, что количество курсов на странице совпадает с количеством курсов в базе данных
        $this->assertCount($courseCount, $courses);
    }

    /**
     * Проверяем детальную страницу курса
     */
    public function testShowCourseWithoutAuthentication(): void
    {
        $client = $this->getClient();

        // Получаем репозиторий для курсов
        $courseRepo = self::$container->get(CourseRepository::class);
        // Находим первый доступный курс
        $course = $courseRepo->findOneBy([]);

        // Если курс не найден, то пропускаем тест
        if (!$course) {
            $this->markTestSkipped('No courses found in the database.');
        }

        // Получаем ID курса
        $courseId = $course->getId();

        // Выполняем GET-запрос на детальную страницу курса
        $client->request('GET', '/courses/' . $courseId);

        // Проверяем, что получен HTTP-статус 200 (Успешный запрос)
        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    /*
     * Проверяем детальную урока, должна открыватся страница логина, если неавторизован
     */
    public function testShowLessonWithoutAuthentication(): void
    {
        $client = $this->getClient();

        // Получаем репозиторий для курсов
        $lessonRepo = self::$container->get(LessonRepository::class);
        // Находим первый доступный курс
        $lesson = $lessonRepo->findOneBy([]);

        // Если урок не найден, то пропускаем тест
        if (!$lesson) {
            $this->markTestSkipped('No courses found in the database.');
        }

        $lessonId = $lesson->getId();

        $client->request('GET', '/lessons/'.$lessonId);

        $response = $client->getResponse();
        $this->assertSame(302, $response->getStatusCode());
        $this->assertSame('/login', $response->headers->get('Location'));

    }

    /*
     * Проверка создания нового курса
     * TODO доработать валидацию формы
     */
    public function testCourseCreation(): void
    {
        $client = $this->getClient();
        $crawler = $client->request('GET', '/courses/new');

//        $this->assertResponseIsSuccessful();
//        $this->assertSelectorTextContains('h1', 'Новый курс');
//
//        // Попытаемся отправить форму с валидными данными
//        $crawler = $client->submitForm('Сохранить', [
//            'course[title]' => 'Test Course1',
//            'course[description]' => 'Test Course Description1',
//        ]);

        // Проверяем, что был выполнен редирект на страницу авторизации
        $this->assertResponseRedirects('/login');
    }

//    /*
//     * Проверка создания нового урока
//     * TODO доработать валидацию формы
//     */
//    public function testLessonCreation(): void
//    {
//        $client = $this->getClient();
//
//        // Получаем репозиторий для курсов
//        $courseRepo = self::$container->get(CourseRepository::class);
//        // Находим первый доступный курс
//        $course = $courseRepo->findOneBy([]);
//        // Получаем ID курса
//        $courseId = $course->getId();
//
//        // Делаем запрос на страницу создания нового урока для данного курса
//        $crawler = $client->request('GET', '/lessons/new/' . $courseId);
//
//        // Проверяем, что страница открывается без ошибок
//        $this->assertResponseIsSuccessful();
//        $this->assertSelectorTextContains('h1', 'Новый урок');
//
//        // Попытаемся отправить форму с валидными данными
//        $crawler = $client->submitForm('Сохранить', [
//            'lesson[title]' => 'Test Lesson',
//            'lesson[content]' => 'Test Lesson Content',
//            'lesson[number]' => 799,
//        ]);
//
//        // Проверяем, что был выполнен редирект на страницу просмотра курса
//        $this->assertResponseRedirects('/courses/'.$courseId);
//    }
//
//    /*
//     * проверяем добавление курса
//     */
//    public function testCourseAdd(): void
//    {
//        $client = $this->getClient();
//
//        // Получаем список курсов и запоминаем, сколько было элементов
//        $crawler = $client->request('GET', '/courses/');
//        $initialCourseCount = $crawler->filter('.col-md-6 .card.mb-4')->count();
//
//        // Делаем запрос на страницу создания нового курса
//        $crawler = $client->request('GET', '/courses/new');
//
//        // Проверяем, что страница открывается без ошибок
//        $this->assertResponseIsSuccessful();
//        $this->assertSelectorTextContains('h1', 'Новый курс');
//
//        // Отправляем форму с валидными данными
//        $crawler = $client->submitForm('Сохранить', [
//            'course[title]' => 'Test Course',
//            'course[description]' => 'Test Course Description',
//        ]);
//
//        // Проверяем, что был выполнен редирект на страницу курсов
//        $this->assertResponseRedirects('/courses/');
//
//        // Переходим на страницу курсов и проверяем, что количество курсов увеличилось
//        $crawler = $client->request('GET', '/courses/');
//        $newCourseCount = $crawler->filter('.col-md-6 .card.mb-4')->count();
//        $this->assertEquals($initialCourseCount + 1, $newCourseCount);
//    }
//
//    /*
//     * проверяем добавление уроков
//     */
//    public function testLessonAdd(): void
//    {
//        $client = $this->getClient();
//
//        // Получаем репозиторий для курсов
//        $courseRepo = self::$container->get(CourseRepository::class);
//        // Находим первый доступный курс
//        $course = $courseRepo->findOneBy([]);
//        // Получаем ID курса
//        $courseId = $course->getId();
//
//        // Получаем страницу курса и приавязанные к нему уроки
//        $crawler = $client->request('GET', '/courses/' . $courseId);
//
//        $initialCourseCount = $crawler->filter('li')->count();
//
//        // Делаем запрос на страницу создания нового урока для данного курса
//        $crawler = $client->request('GET', '/lessons/new/' . $courseId);
//
//        // Проверяем, что страница открывается без ошибок
//        $this->assertResponseIsSuccessful();
//        $this->assertSelectorTextContains('h1', 'Новый урок');
//
//        // Попытаемся отправить форму с валидными данными
//        $crawler = $client->submitForm('Сохранить', [
//            'lesson[title]' => 'Test new Lesson',
//            'lesson[content]' => 'Test new Lesson Content',
//            'lesson[number]' => 799,
//        ]);
//
//        // Проверяем, что был выполнен редирект на детальную страницу курса
//        $this->assertResponseRedirects('/courses/' . $courseId);
//
//        // Получаем страницу курса и проверяем изменилось ли количество
//        $crawler = $client->request('GET', '/courses/' . $courseId);
//        $newCourseCount = $crawler->filter('li')->count();
//        $this->assertEquals($initialCourseCount + 1, $newCourseCount);
//    }


    protected function getFixtures(): array
    {
        return [
            CourseFixtures::class
        ];
    }
}
